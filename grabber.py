import os
import sys
import datetime
import time
import argparse
import logging
import json
import requests
import pytz
import shutil

#  --------------------------------------------------------------------------- #
# Functions
# -----------------------------------------------------------------------------#

# --------------------------------------------------------------------------- #
# Main
# --------------------------------------------------------------------------- #
if __name__ == "__main__":
    # --------------------------------------------------------------------------- #
    # Configuration file
    # --------------------------------------------------------------------------- #
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-c', help='configuration file')
    arg_parser.add_argument('-l', help='log file (optional, if empty log redirected on stdout)')
    args = arg_parser.parse_args()

    # Load the main parameters
    config_file = args.c
    try:
        if os.path.isfile(config_file) is False:
            print('\nATTENTION! Unable to open configuration file %s\n' % config_file)
            sys.exit(1)
    except Exception as e:
        print('\nATTENTION! Input arg -c not set\n')
        sys.exit(2)

    cfg = json.loads(open(args.c).read())

    # --------------------------------------------------------------------------- #
    # Set logging object
    # --------------------------------------------------------------------------- #
    if not args.l:
        log_file = None
    else:
        log_file = args.l

    logger = logging.getLogger()
    logging.basicConfig(format='%(asctime)-15s::%(levelname)s::%(funcName)s::%(message)s', level=logging.INFO,
                        filename=log_file)

    # --------------------------------------------------------------------------- #
    # Starting program
    # --------------------------------------------------------------------------- #
    logger.info("Starting program")

    creds = (cfg['grafana']['user'], cfg['grafana']['password'])

    for download in cfg['downloads']:

        start_dt = datetime.datetime.strptime(download['from'], '%Y-%m-%d %H:%M:%S')
        start_dt = pytz.utc.localize(start_dt)
        start_ts_ms = int(start_dt.timestamp() * 1e3)

        end_dt = datetime.datetime.strptime(download['to'], '%Y-%m-%d %H:%M:%S')
        end_dt = pytz.utc.localize(end_dt)
        end_ts_ms = int(end_dt.timestamp()*1e3)

        panels = list(range(download['panelsIds'][0], download['panelsIds'][1]+1))
        for panel in panels:
            logger.info('Try to grab image related to panel %i' % panel)

            img_file_name = '%s/%s_panel_%i.png' % (download['outputFolder'], download['label'], panel)

            url = '%s%s?orgId=%i&from=%i&to=%i&panelId=%i&width=%i&height=%i&tz=%s' % (cfg['grafana']['url'],
                                                                                       download['dashboard'],
                                                                                       cfg['grafana']['orgId'],
                                                                                       start_ts_ms,
                                                                                       end_ts_ms,
                                                                                       panel,
                                                                                       download['width'],
                                                                                       download['height'],
                                                                                       download['timeZone'])
            url = url.replace('/d/', '/render/d-solo/')

            logger.info('Downloading image from %s to %s' % (url, img_file_name))
            try:
                r = requests.get(url=url, auth=creds, stream=True, timeout=cfg['grafana']['timeout'])
                r.raw.decode_content = True
                fw = open(img_file_name, 'wb')
                shutil.copyfileobj(r.raw, fw)
                fw.close()
                logger.info('Image saved in file %s' % img_file_name)
                time.sleep(1)
            except Exception as e:
                logger.error('EXCEPTION: %s' % str(e))

    logger.info("Ending program")
